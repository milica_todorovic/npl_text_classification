import nltk.classify
from sklearn.model_selection import train_test_split
import pandas as pd
from nltk import NaiveBayesClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import confusion_matrix, classification_report


def generate_features_tf(vector, line, feature_names):
    """
    Create a dictionary in form: "word": TF word measure
    :param vector: Feature vector extracted via BOW
    :param line: Corresponding dataset sentence
    :param feature_names: Corresponding words
    :return:
    """
    tf_matrix = dict()
    line_len = len(line)

    # For each word in the feature vector,
    # find the word occurrence,
    # divide by number of words in the corresponding sentence for TF measure

    # TF(t,d) = occurrence of t in d / number of words in d
    for i, occurrence in enumerate(vector):
        word = feature_names[i]

        tf_matrix[word] = occurrence / line_len

    return tf_matrix


def generate_feature_vectors(features_bow, feature_names, x):
    """
    Generate feature vectors from the features extracted via BOW, using TF measure
    :param features_bow: Features extracted via BOW
    :param feature_names: Corresponding words
    :param x: Initial dataset sentences
    :return:
    """
    data = []
    for i in range(len(x)):
        vector = []
        for j in range(features_bow.shape[1]):
            vector.append((features_bow[i, j]))
        data.append(generate_features_tf(vector, x[i], feature_names))

    return data


def generate_feature_set(X, y):
    """
    Create feature set in the form [(feature_vector, label)]
    :param X: feature vectors
    :param y: labels
    :return: feature set
    """
    feature_set = []
    for i in range(len(X)):
        feature_set.append((X[i], y[i]))
    return feature_set


def main():
    # Load dataset
    dataset = pd.read_csv("./cleaned_trans/friends_lines_clean.csv")
    x = dataset['Lines'].values
    y = dataset['Character'].values

    # Count words - Bag of Words
    extract_bow = CountVectorizer(max_features=50)

    # Bag of words extracted features and feature names
    features_bow = extract_bow.fit_transform(x)
    feature_names = extract_bow.get_feature_names()

    # Transform the features representation to feature vectors and use TF measure
    data = generate_feature_vectors(features_bow, feature_names, x)

    # Split generated dataset to train and test
    x_train, x_test, y_train, y_test = train_test_split(data, y, test_size=0.2, random_state=55)

    # Create feature sets by appending labels to feature vectors
    train = generate_feature_set(x_train, y_train)
    test = generate_feature_set(x_test, y_test)

    # Train the classifier
    classifier = NaiveBayesClassifier.train(train)

    # Score the classification
    predicted = classifier.classify_many(x_test)
    print('Rachel', 'Ross', 'Monica', 'Joey', 'Phoebe', 'Chandler')
    print(confusion_matrix(predicted, y_test))
    print(classification_report(y_true=y_test, y_pred=predicted))
    print("Accuracy", nltk.classify.accuracy(classifier, test))

    # Classify new sentences
    lines = [x[0], x[57], x[1665]]
    said_by = [y[0], y[57], y[1665]]
    data = generate_feature_vectors(features_bow, feature_names, lines)
    pred = classifier.classify_many(data)

    print("----------------------------------PREDICTIONS----------------------------------")
    for ind, line in enumerate(lines):
        print("Line: ", line)
        print("Predicted said by: ", pred[ind])
        print("Original said by: ", said_by[ind])
        print("-------------------------------------------------------------------------------")


if __name__ == '__main__':
    main()
