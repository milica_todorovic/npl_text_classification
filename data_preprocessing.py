import pandas as pd
import re
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from autocorrect import Speller
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')


def use_word(word, stop_words):
    """
    Use word if it is not a stop word and longer than 1
    :param word:
    :param stop_words:
    :return:
    """
    return word not in stop_words and len(word) > 1


def clean_line(line, speller, stemmer):
    # leave only letters
    line = re.sub('[^A-Za-z]', ' ', line)
    # covert all letters to lower case
    line = line.lower()

    # tokenize the line
    tokenized_line = word_tokenize(line)

    processed_line = []
    sw_set = set(stopwords.words('english'))
    for word in tokenized_line:
        if use_word(word, sw_set):
            # stem the word
            stemmed_word = stemmer.stem(word)
            # attempt auto correct
            auto_corr_word = speller(stemmed_word)
            processed_line.append(auto_corr_word)

    # join words into a single sentence
    processed_line_text = " ".join(processed_line)
    return processed_line_text


def clean_dataset_lines(lines):
    stemmer = PorterStemmer()
    speller = Speller(lang='en')

    cleaned_dataset = []

    for line in lines:
        processed_line = clean_line(line, speller, stemmer)
        cleaned_dataset.append(processed_line)

    return cleaned_dataset


def preprocess_dataset():
    # Load dataset
    dataset = pd.read_csv("./cleaned_trans/friends_lines.csv")
    print(dataset.head(5))

    lines = dataset['Lines'].values
    characters = dataset['Character'].values

    # Clean dataset
    cleaned_dataset = clean_dataset_lines(lines)
    print(cleaned_dataset[:5])

    character_lines_dict = {
        'Lines': [],
        'Character': []
    }

    # Remove blank lines after cleaning
    for i in range(len(cleaned_dataset)):
        if cleaned_dataset[i]:
            character_lines_dict['Lines'].append(cleaned_dataset[i])
            character_lines_dict['Character'].append(characters[i])

    # Save to file
    cleaned_df = pd.DataFrame(character_lines_dict)
    cleaned_df.to_csv("./cleaned_trans/friends_lines_clean.csv", index=False)


if __name__ == '__main__':
    preprocess_dataset()


