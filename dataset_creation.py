import re
import os
import pandas as pd


def read_lines_from_file(file_path):
    trans_file = open(file_path, 'r')
    trans_file_lines = trans_file.readlines()
    trans_file.close()

    return trans_file_lines


def clean_line(line):
    # remove text between (..)
    line = re.sub(r'\(.+?\)', '', line)
    # remove text between [..]
    line = re.sub(r'\[.+?\]', '', line)
    line = ' '.join(line.split())

    return line


def add_lines_to_character(character_list, trans_file_lines, character_lines_dict):
    # Lines in the transcript are in the form 'Character list' : 'Line'
    for line in trans_file_lines:
        # Find ":" - separator between character and line
        said_by_index = line.find(':')
        if said_by_index != -1:
            # Each line can potentially be said (at the same time) by multiple characters
            said_by_list = line[:said_by_index]
            # In this case remove separators to get all the character names
            said_by_list.replace(',', '')
            said_by_list.replace('and', '')
            said_by_list = said_by_list.split()
            for said_by in said_by_list:
                # if the character is a main character
                if said_by in character_list:
                    # extract the line
                    said_line = line[said_by_index + 1:]
                    # clean line
                    said_line = clean_line(said_line)
                    #
                    if said_line:
                        # append line and character name to corresponding lists
                        character_lines_dict['Lines'].append(said_line)
                        character_lines_dict['Character'].append(said_by)


def preprocess_transcripts():
    character_lines_dict = {
        'Lines': [],
        'Character': []
    }

    # main character names
    character_list = ['Rachel', 'Ross', 'Monica', 'Joey', 'Phoebe', 'Chandler']

    # walk all the transcript files and extract the lines
    for trans_file in os.listdir("./original_trans"):
        trans_file_path = f'./original_trans/{trans_file}'
        all_lines = read_lines_from_file(trans_file_path)
        add_lines_to_character(character_list, all_lines, character_lines_dict)

    # save extracted to new dataset
    lines_df = pd.DataFrame(character_lines_dict)
    lines_df.to_csv(path_or_buf="./cleaned_trans/friends_lines.csv", index=False)


if __name__ == '__main__':
    preprocess_transcripts()
